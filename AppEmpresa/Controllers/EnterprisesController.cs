﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEmpresa.Core.Dto;
using AppEmpresa.Core.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppEmpresa.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    
    public class EnterprisesController : ControllerBase
    {

        [HttpGet]
        [Route("{id}")]
        [Authorize]
        public string Get(int id)
        {
            return "value";
        }
        [HttpGet]
        [Authorize]
        public string GetEnterprise([FromQuery] EnterpriseDto enterpriseDto)
        {
            return "value";
        }

    }
}
