﻿using AppEmpresa.Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppEmpresa.Repository.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Enterprise>().HasData(
                new Enterprise { Id = 1, Name = "Ridiculus Mus Limited", EnterpriseTypes = 1 },
                new Enterprise { Id = 2, Name = "Vitae Odio Ltd", EnterpriseTypes = 2 },
                new Enterprise { Id = 3, Name = "Ac Associates", EnterpriseTypes = 1 },
                new Enterprise { Id = 4, Name = "aQm", EnterpriseTypes = 1 }
            );

            //modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            //{
            //    Id = "1",
            //    Name = "admin",
            //    NormalizedName = "admin"
            //});

            //var hasher = new PasswordHasher<User>();
            //modelBuilder.Entity<User>().HasData(new User
            //{
            //    Id = "1",                
            //    UserName = "testeapple",
            //    Email = " testeapple@ioasys.com.br",
            //    NormalizedUserName = "teste apple",
            //    EmailConfirmed = true,
            //    PasswordHash = hasher.HashPassword(null, "12341234"),
            //    SecurityStamp = string.Empty
            //});

            //modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            //{
            //    RoleId = "1",
            //    UserId = "1"
            ///});

        }

    }
}
