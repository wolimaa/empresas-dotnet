﻿using System.Threading.Tasks;
using AppEmpresa.Core.Entities;
using AppEmpresa.Core.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace AppEmpresa.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> userManager;
        private readonly IdentityDbContext<User> context;

        public UserRepository(IdentityDbContext<User> context, UserManager<User> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public Task<User> GetIdentityAsync(string username, string password)
        {
            throw new System.NotImplementedException();
        }
    }
}