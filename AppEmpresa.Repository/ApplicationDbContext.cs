﻿using AppEmpresa.Core.Entities;
using AppEmpresa.Repository.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AppEmpresas.Repository
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<Enterprise> Enterprises { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Enterprise>().HasData(
               new Enterprise { Id = 1, Name = "Ridiculus Mus Limited", EnterpriseTypes = 1 },
               new Enterprise { Id = 2, Name = "Vitae Odio Ltd", EnterpriseTypes = 2 },
               new Enterprise { Id = 3, Name = "Ac Associates", EnterpriseTypes = 1 },
               new Enterprise { Id = 4, Name = "aQm", EnterpriseTypes = 1 }
           );

        }
    }
}