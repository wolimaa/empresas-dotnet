﻿using AppEmpresa.Core.Entities;
using AppEmpresas.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEmpresa.Repository
{
    public class ApplicationDbContextInitializer
    {
        private ApplicationDbContext context;

        private List<Enterprise> enterprises = new List<Enterprise>{
               new Enterprise { Name = "Ridiculus Mus Limited", EnterpriseTypes = 1 },
               new Enterprise { Name = "Vitae Odio Ltd", EnterpriseTypes = 2 },
               new Enterprise { Name = "Ac Associates", EnterpriseTypes = 1 },
               new Enterprise { Name = "aQm", EnterpriseTypes = 1 }
        };

        private User user = new User
        {
            Email = "testeapple@ioasys.com.br",
            UserName = "testeapple",
            EmailConfirmed = true,
        };

        public ApplicationDbContextInitializer(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task Seed()
        {
            if (!context.Enterprises.Any())
            {
                context.Enterprises.AddRange(enterprises);
                await context.SaveChangesAsync();
            }

            if (!context.Users.Any(u => u.Email == user.Email))
            {
                var password = new PasswordHasher<User>();
                var hashed = password.HashPassword(user, "12341234");
                user.PasswordHash = hashed;
                var userStore = new UserStore<User>(context);
                var result = userStore.CreateAsync(user);
                await context.SaveChangesAsync();
            }
        }
    }
}