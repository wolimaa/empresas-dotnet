﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppEmpresa.Core.Dto
{
    public class EnterpriseDto
    {
        public string Name { get; set; }

        public int EnterpriseTypes { get; set; }
    }
}
