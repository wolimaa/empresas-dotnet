﻿using System.Threading.Tasks;

namespace AppEmpresa.Core.Provider
{
    public interface ITokenProviderMiddleware
    {
        Task Invoke(Microsoft.AspNetCore.Http.HttpContext context);
    }
}