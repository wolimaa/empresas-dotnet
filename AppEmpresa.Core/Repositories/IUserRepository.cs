﻿using AppEmpresa.Core.Entities;
using System.Threading.Tasks;

namespace AppEmpresa.Core.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetIdentityAsync(string username, string password);
    }
}