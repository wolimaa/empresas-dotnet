﻿using System.ComponentModel.DataAnnotations;

namespace AppEmpresa.Core.Entities
{
    public class Enterprise
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int EnterpriseTypes { get; set; }
    }
}